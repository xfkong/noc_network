`timescale 1ns/1ps
`define CLOCK_PERIOD 10

module testbench();

		logic			clk;
		logic			reset;

		logic	[0:3]		core_0_req_in;
		logic	[0:3]		core_1_req_in;
		logic	[0:3]		core_2_req_in;
		logic	[0:3]		directory_req_in;
		
   		logic	[0:1]		core_0_dest_rtr_addr_in;
		logic	[0:1]		core_1_dest_rtr_addr_in;
		logic	[0:1]		core_2_dest_rtr_addr_in;
		logic	[0:1]		directory_dest_rtr_addr_in;


		logic	[0:1]		core_0_req_rtr_addr_in;
		logic	[0:1]		core_1_req_rtr_addr_in;
		logic	[0:1]		core_2_req_rtr_addr_in;
		logic	[0:1]		directory_req_rtr_addr_in;


		logic	[0:1]		core_0_ack_count_in;
		logic	[0:1]		core_1_ack_count_in;
		logic	[0:1]		core_2_ack_count_in;
		logic	[0:1]		directory_ack_count_in;

		logic	[0:31]		core_0_addr_in;
		logic	[0:31]		core_1_addr_in;
		logic	[0:31]		core_2_addr_in;
		logic	[0:31]		directory_addr_in;

		logic	[0:63]		core_0_data_in;
		logic	[0:63]		core_1_data_in;
		logic	[0:63]		core_2_data_in;
		logic	[0:63]		directory_data_in;
		
		// core 0
		logic	[0:3]		core_0_router2proc_VC1_cmd_out;
		logic	[0:31]		core_0_router2proc_VC1_addr_out;
		logic	[0:1]		core_0_router2proc_VC1_req_out;
		logic	[0:3]		core_0_router2proc_VC2_cmd_out;
		logic	[0:31]		core_0_router2proc_VC2_addr_out;
		logic	[0:63]		core_0_router2proc_VC2_data_out;
		logic	[0:1]		core_0_router2proc_VC2_ack_out;

		// core 1
		logic	[0:3]		core_1_router2proc_VC1_cmd_out;
		logic	[0:31]		core_1_router2proc_VC1_addr_out;
		logic	[0:1]		core_1_router2proc_VC1_req_out;
		logic	[0:3]		core_1_router2proc_VC2_cmd_out;
		logic	[0:31]		core_1_router2proc_VC2_addr_out;
		logic	[0:63]		core_1_router2proc_VC2_data_out;
		logic	[0:1]		core_1_router2proc_VC2_ack_out;
	
		// core 2
		logic	[0:3]		core_2_router2proc_VC1_cmd_out;
		logic	[0:31]		core_2_router2proc_VC1_addr_out;
		logic	[0:1]		core_2_router2proc_VC1_req_out;
		logic	[0:3]		core_2_router2proc_VC2_cmd_out;
		logic	[0:31]		core_2_router2proc_VC2_addr_out;
		logic	[0:63]		core_2_router2proc_VC2_data_out;
		logic	[0:1]		core_2_router2proc_VC2_ack_out;

		// directory
		logic	[0:3]		directory_router2dir_VC0_cmd_out;
		logic	[0:31]		directory_router2dir_VC0_addr_out;
		logic	[0:1]		directory_router2dir_VC0_req_out;		
		logic	[0:63]		directory_router2dir_VC0_data_out;

		logic	[0:3]		directory_router2dir_VC2_cmd_out;
		logic	[0:31]		directory_router2dir_VC2_addr_out;
		logic	[0:1]		directory_router2dir_VC2_req_out;		
		logic	[0:63]		directory_router2dir_VC2_data_out;

		//Debug
		logic	[0:69-1]	core_0_channel_send;
		logic 	[0:69-1] 	channel_router_0_op_0;
		logic 	[0:69-1] 	channel_router_0_op_1;
		logic 	[0:69-1] 	channel_router_0_op_2;
		logic 	[0:69-1] 	channel_router_0_op_3;
		logic 	[0:69-1] 	channel_router_1_op_3;

		integration integration_0 (
				.clk					(clk),
				.reset					(reset),

				.core_0_req_in				(core_0_req_in),
				.core_1_req_in				(core_1_req_in),
				.core_2_req_in				(core_2_req_in),
				.directory_req_in			(directory_req_in),
					
   				.core_0_dest_rtr_addr_in		(core_0_dest_rtr_addr_in),
				.core_1_dest_rtr_addr_in		(core_1_dest_rtr_addr_in),
				.core_2_dest_rtr_addr_in		(core_2_dest_rtr_addr_in),
				.directory_dest_rtr_addr_in		(directory_dest_rtr_addr_in),

				.core_0_req_rtr_addr_in			(core_0_req_rtr_addr_in),
				.core_1_req_rtr_addr_in			(core_1_req_rtr_addr_in),
				.core_2_req_rtr_addr_in			(core_2_req_rtr_addr_in),
				.directory_req_rtr_addr_in		(directory_req_rtr_addr_in),

				.core_0_ack_count_in			(core_0_ack_count_in),
				.core_1_ack_count_in			(core_1_ack_count_in),
				.core_2_ack_count_in			(core_2_ack_count_in),
				.directory_ack_count_in			(directory_ack_count_in),

				.core_0_addr_in				(core_0_addr_in),
				.core_1_addr_in				(core_1_addr_in),	
				.core_2_addr_in				(core_2_addr_in),
				.directory_addr_in			(directory_addr_in),

				.core_0_data_in				(core_0_data_in),
				.core_1_data_in				(core_1_data_in),
				.core_2_data_in				(core_2_data_in),
				.directory_data_in			(directory_data_in),
		
				// core 0
				.core_0_router2proc_VC1_cmd_out		(core_0_router2proc_VC1_cmd_out),
				.core_0_router2proc_VC1_addr_out	(core_0_router2proc_VC1_addr_out),
				.core_0_router2proc_VC1_req_out		(core_0_router2proc_VC1_req_out),
				.core_0_router2proc_VC2_cmd_out		(core_0_router2proc_VC2_cmd_out),
				.core_0_router2proc_VC2_addr_out	(core_0_router2proc_VC2_addr_out),
				.core_0_router2proc_VC2_data_out	(core_0_router2proc_VC2_data_out),
				.core_0_router2proc_VC2_ack_out		(core_0_router2proc_VC2_ack_out),

				// core 1
				.core_1_router2proc_VC1_cmd_out		(core_1_router2proc_VC1_cmd_out),
				.core_1_router2proc_VC1_addr_out	(core_1_router2proc_VC1_addr_out),
				.core_1_router2proc_VC1_req_out		(core_1_router2proc_VC1_req_out),
				.core_1_router2proc_VC2_cmd_out		(core_1_router2proc_VC2_cmd_out),
				.core_1_router2proc_VC2_addr_out	(core_1_router2proc_VC2_addr_out),
				.core_1_router2proc_VC2_data_out	(core_1_router2proc_VC2_data_out),
				.core_1_router2proc_VC2_ack_out		(core_1_router2proc_VC2_ack_out),
	
				// core 2
				.core_2_router2proc_VC1_cmd_out		(core_2_router2proc_VC1_cmd_out),
				.core_2_router2proc_VC1_addr_out	(core_2_router2proc_VC1_addr_out),
				.core_2_router2proc_VC1_req_out		(core_2_router2proc_VC1_req_out),
				.core_2_router2proc_VC2_cmd_out		(core_2_router2proc_VC2_cmd_out),
				.core_2_router2proc_VC2_addr_out	(core_2_router2proc_VC2_addr_out),
				.core_2_router2proc_VC2_data_out	(core_2_router2proc_VC2_data_out),
				.core_2_router2proc_VC2_ack_out		(core_2_router2proc_VC2_ack_out),

				// directory
				.directory_router2dir_VC0_cmd_out	(directory_router2dir_VC0_cmd_out),	
				.directory_router2dir_VC0_addr_out	(directory_router2dir_VC0_addr_out),
				.directory_router2dir_VC0_req_out	(directory_router2dir_VC0_req_out),		
				.directory_router2dir_VC0_data_out	(directory_router2dir_VC0_data_out),
		
				.directory_router2dir_VC2_cmd_out	(directory_router2dir_VC2_cmd_out),	
				.directory_router2dir_VC2_addr_out	(directory_router2dir_VC2_addr_out),
				.directory_router2dir_VC2_req_out	(directory_router2dir_VC2_req_out),		
				.directory_router2dir_VC2_data_out	(directory_router2dir_VC2_data_out),

				//Debug
				.core_0_channel_send			(core_0_channel_send),
				.channel_router_0_op_0			(channel_router_0_op_0),
				.channel_router_0_op_1			(channel_router_0_op_1),
				.channel_router_0_op_2			(channel_router_0_op_2),
				.channel_router_0_op_3			(channel_router_0_op_3),
				.channel_router_1_op_3			(channel_router_1_op_3)
		
			);

	always begin
    		#(`CLOCK_PERIOD/2.0);
    		clk = ~clk;
  	end

	initial begin
    		clk	= 1'b0;
    		reset 	= 1'b1;
		
		core_0_req_in	 = 4'h0;
		core_1_req_in	 = 4'h0;
		core_2_req_in	 = 4'h0;
		directory_req_in = 4'h0;	
		
   		core_0_dest_rtr_addr_in	    = 2'b00;
		core_1_dest_rtr_addr_in	    = 2'b00;
		core_2_dest_rtr_addr_in	    = 2'b00;
		directory_dest_rtr_addr_in  = 2'b00;

		core_0_req_rtr_addr_in	    = 2'b00;
		core_1_req_rtr_addr_in	    = 2'b00;
		core_2_req_rtr_addr_in      = 2'b00;
		directory_req_rtr_addr_in   = 2'b00;

		core_0_ack_count_in	    = 2'b00;
		core_1_ack_count_in	    = 2'b00;
		core_2_ack_count_in	    = 2'b00;
		directory_ack_count_in 	    = 2'b00;

		core_0_addr_in		    = 32'h0;
		core_1_addr_in		    = 32'h0;
		core_2_addr_in		    = 32'h0;
		directory_addr_in	    = 32'h0;

		core_0_data_in		    = 64'h0;
		core_1_data_in		    = 64'h0;
		core_2_data_in		    = 64'h0;
		directory_data_in	    = 64'h0;
	
    		// reset will be deasserted after 10 cycles
    		repeat(10) @(negedge clk);
   		reset 	= 1'b0;
		repeat(10) @(negedge clk);
		core_0_req_in 		= 4'b1010;
		core_0_dest_rtr_addr_in	= 2'b11;
		core_0_addr_in		= 32'hFFFF_FFFF;
		core_0_data_in		= 64'hAAAA_AAAA_AAAA_AAAA;
/*
		core_1_req_in 		= 4'b0001;
		core_1_dest_rtr_addr_in	= 2'b11;
		core_1_addr_in		= 32'hAAAA_AAAA;

		core_2_req_in 		= 4'b0001;
		core_2_dest_rtr_addr_in	= 2'b11;
		core_2_addr_in		= 32'hBBBB_BBBB;
*/
		@(negedge clk);
		core_0_req_in 		= 4'b0000;
		core_0_dest_rtr_addr_in	= 2'b00;
		core_0_addr_in		= 32'h0;
		core_0_data_in		= 64'h0;
/*
		core_1_req_in 		= 4'b0000;
		core_1_dest_rtr_addr_in	= 2'b00;
		core_1_addr_in		= 32'h0;

		core_2_req_in 		= 4'b0000;
		core_2_dest_rtr_addr_in	= 2'b11;
		core_2_addr_in		= 32'h0000_0000;
*/
		$display("Complete");
   
    		#10000 $finish;
  	end
   
endmodule
