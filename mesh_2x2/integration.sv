/*         
// number of VCs
localparam 	num_vcs = 4;
   
// width required to select individual VC
localparam 	vc_idx_width = clogb(num_vcs);
      
// width of flow control signals
localparam 	flow_ctrl_width = 1 + vc_idx_width;
   	 		 	   
// width of link management signals
localparam 	link_ctrl_width = enable_link_pm ? 1 : 0;
   
// width of flit control signals
localparam 	flit_ctrl_width = 15;
   
// channel width
localparam 	channel_width = link_ctrl_width + flit_ctrl_width + flit_data_width;



typedef enum logic [3:0] {
	CMD_IDLE	= 4'b0000,
  	GetS        	= 4'b0001,
  	GetM		= 4'b0010,
  	PutS		= 4'b0011,
  	PutM		= 4'b0100,
	PutE		= 4'b0101,
  	Fwd_GetS	= 4'b0110,
  	Fwd_GetM	= 4'b0111,
	Invalid		= 4'b1000,
	Put_Ack		= 4'b1001,
	Data		= 4'b1010,
	Inv_Ack		= 4'b1011
} INTERFACE_CMD;

*/
   
module integration (
		input			clk,
		input			reset,

		input	[0:3]		core_0_req_in,
		input	[0:3]		core_1_req_in,
		input	[0:3]		core_2_req_in,
		input	[0:3]		directory_req_in,

		
   		input	[0:1]		core_0_dest_rtr_addr_in,
		input	[0:1]		core_1_dest_rtr_addr_in,
		input	[0:1]		core_2_dest_rtr_addr_in,
		input	[0:1]		directory_dest_rtr_addr_in,


		input	[0:1]		core_0_req_rtr_addr_in,
		input	[0:1]		core_1_req_rtr_addr_in,
		input	[0:1]		core_2_req_rtr_addr_in,
		input	[0:1]		directory_req_rtr_addr_in,


		input	[0:1]		core_0_ack_count_in,
		input	[0:1]		core_1_ack_count_in,
		input	[0:1]		core_2_ack_count_in,
		input	[0:1]		directory_ack_count_in,

		input	[0:31]		core_0_addr_in,
		input	[0:31]		core_1_addr_in,
		input	[0:31]		core_2_addr_in,
		input	[0:31]		directory_addr_in,

		input	[0:63]		core_0_data_in,
		input	[0:63]		core_1_data_in,
		input	[0:63]		core_2_data_in,
		input	[0:63]		directory_data_in,
/*		
		output	logic		core_0_valid_packet_out,
		output	logic		core_1_valid_packet_out,
		output	logic		core_2_valid_packet_out,
		output	logic		directory_valid_packet_out,

		output	logic	[0:3]	core_0_req_out,
		output	logic	[0:3]	core_1_req_out,
		output	logic	[0:3]	core_2_req_out,
		output	logic	[0:3]	directory_req_out,

		output	logic	[0:31]	core_0_addr_out,
		output	logic	[0:31]	core_1_addr_out,
		output	logic	[0:31]	core_2_addr_out,
		output	logic	[0:31]	directory_addr_out,
		
		output	logic	[0:63]	core_0_data_out,
		output	logic	[0:63]	core_1_data_out,
		output	logic	[0:63]	core_2_data_out,
		output	logic	[0:63]	directory_data_out,

		output	logic	[0:1]	core_0_packet_vc_idx_out,
		output	logic	[0:1]	core_1_packet_vc_idx_out,
		output	logic	[0:1]	core_2_packet_vc_idx_out,
		output	logic	[0:1]	directory_packet_vc_idx_out,

		output	logic	[0:1]	core_0_src_rtr_addr_out,
		output	logic	[0:1]	core_1_src_rtr_addr_out,
		output	logic	[0:1]	core_2_src_rtr_addr_out,
		output	logic	[0:1]	directory_src_rtr_addr_out,

		output	logic	[0:1]	core_0_req_rtr_addr_out,
		output	logic	[0:1]	core_1_req_rtr_addr_out,
		output	logic	[0:1]	core_2_req_rtr_addr_out,
		output	logic	[0:1]	directory_req_rtr_addr_out,

		output	logic	[0:1]	core_0_ack_cnt_out,
		output	logic	[0:1]	core_1_ack_cnt_out,
		output	logic	[0:1]	core_2_ack_cnt_out,
		output	logic	[0:1]	directory_ack_cnt_out,
*/

		// core 0
		output	logic	[0:3]	core_0_router2proc_VC1_cmd_out,
		output	logic	[0:31]	core_0_router2proc_VC1_addr_out,
		output	logic	[0:1]	core_0_router2proc_VC1_req_out,
		output	logic	[0:3]	core_0_router2proc_VC2_cmd_out,
		output	logic	[0:31]	core_0_router2proc_VC2_addr_out,
		output	logic	[0:63]	core_0_router2proc_VC2_data_out,
		output	logic	[0:1]	core_0_router2proc_VC2_ack_out,

		// core 1
		output	logic	[0:3]	core_1_router2proc_VC1_cmd_out,
		output	logic	[0:31]	core_1_router2proc_VC1_addr_out,
		output	logic	[0:1]	core_1_router2proc_VC1_req_out,
		output	logic	[0:3]	core_1_router2proc_VC2_cmd_out,
		output	logic	[0:31]	core_1_router2proc_VC2_addr_out,
		output	logic	[0:63]	core_1_router2proc_VC2_data_out,
		output	logic	[0:1]	core_1_router2proc_VC2_ack_out,
	
		// core 2
		output	logic	[0:3]	core_2_router2proc_VC1_cmd_out,
		output	logic	[0:31]	core_2_router2proc_VC1_addr_out,
		output	logic	[0:1]	core_2_router2proc_VC1_req_out,
		output	logic	[0:3]	core_2_router2proc_VC2_cmd_out,
		output	logic	[0:31]	core_2_router2proc_VC2_addr_out,
		output	logic	[0:63]	core_2_router2proc_VC2_data_out,
		output	logic	[0:1]	core_2_router2proc_VC2_ack_out,

		// directory
		output	logic	[0:3]	directory_router2dir_VC0_cmd_out,
		output	logic	[0:31]	directory_router2dir_VC0_addr_out,
		output	logic	[0:1]	directory_router2dir_VC0_req_out,		
		output	logic	[0:63]	directory_router2dir_VC0_data_out,

		output	logic	[0:3]	directory_router2dir_VC2_cmd_out,
		output	logic	[0:31]	directory_router2dir_VC2_addr_out,
		output	logic	[0:1]	directory_router2dir_VC2_req_out,		
		output	logic	[0:63]	directory_router2dir_VC2_data_out,
			
		//Debug
		output	logic	[0:channel_width-1]	core_0_channel_send,	
		output	logic 	[0:channel_width-1] 	channel_router_0_op_0,
		output	logic 	[0:channel_width-1] 	channel_router_0_op_1,
		output	logic 	[0:channel_width-1] 	channel_router_0_op_2,
		output	logic 	[0:channel_width-1] 	channel_router_0_op_3,
		output	logic 	[0:channel_width-1] 	channel_router_1_op_3
		
	);


		logic	[0:3]			rtr_network_error;

		//logic	[0:channel_width-1]	core_0_channel_send;
		logic	[0:channel_width-1]	core_1_channel_send;
		logic	[0:channel_width-1]	core_2_channel_send;
		logic	[0:channel_width-1]	directory_channel_send;

		logic	[0:flow_ctrl_width-1]	core_0_flow_ctrl_send;
		logic	[0:flow_ctrl_width-1]	core_1_flow_ctrl_send;
		logic	[0:flow_ctrl_width-1]	core_2_flow_ctrl_send;
		logic	[0:flow_ctrl_width-1]	directory_flow_ctrl_send;
	
		logic	[0:channel_width-1]	core_0_channel_receive;
		logic	[0:channel_width-1]	core_1_channel_receive;
		logic	[0:channel_width-1]	core_2_channel_receive;
		logic	[0:channel_width-1]	directory_channel_receive;

		logic	[0:flow_ctrl_width-1]	core_0_flow_ctrl_receive;
		logic	[0:flow_ctrl_width-1]	core_1_flow_ctrl_receive;
		logic	[0:flow_ctrl_width-1]	core_2_flow_ctrl_receive;
		logic	[0:flow_ctrl_width-1]	directory_flow_ctrl_receive;

	// router connected to core 0
	core_interface network_interface_0 (
 	
   			.clk				(clk),
   			.reset				(reset),
			.error				(rtr_network_error[0]),

			.core_req_in			(core_0_req_in),
			.src_rtr_addr_in		(2'b00),		
   			.dest_rtr_addr_in		(core_0_dest_rtr_addr_in),
			.req_rtr_addr_in		(core_0_req_rtr_addr_in),
			.ack_count_in			(core_0_ack_count_in),
			.cache_addr_in			(core_0_addr_in),		
			.cache_data_in			(core_0_data_in),
   
   			.send_channel_out		(core_0_channel_send),
			.send_flow_ctrl_out		(core_0_flow_ctrl_send),
 			
 			.receive_channel_in		(core_0_channel_receive),
   			.receive_flow_ctrl_in		(core_0_flow_ctrl_receive),

			//VC1
			.router2proc_VC1_cmd_out	(core_0_router2proc_VC1_cmd_out),
			.router2proc_VC1_addr_out	(core_0_router2proc_VC1_addr_out),
			.router2proc_VC1_req_out	(core_0_router2proc_VC1_req_out),
			//VC2		
			.router2proc_VC2_cmd_out	(core_0_router2proc_VC2_cmd_out),
			.router2proc_VC2_addr_out	(core_0_router2proc_VC2_addr_out),
			.router2proc_VC2_data_out	(core_0_router2proc_VC2_data_out),
			.router2proc_VC2_ack_out	(core_0_router2proc_VC2_ack_out)
		);

	// router connected to core 1
	core_interface network_interface_1 (
 	
   			.clk				(clk),
   			.reset				(reset),
			.error				(rtr_network_error[1]),

			.core_req_in			(core_1_req_in),
			.src_rtr_addr_in		(2'b10),		
   			.dest_rtr_addr_in		(core_1_dest_rtr_addr_in),	
			.req_rtr_addr_in		(core_1_req_rtr_addr_in),
			.ack_count_in			(core_1_ack_count_in),
			.cache_addr_in			(core_1_addr_in),
			.cache_data_in			(core_1_data_in),
   
   			.send_channel_out		(core_1_channel_send),
			.send_flow_ctrl_out		(core_1_flow_ctrl_send),
 		
 			.receive_channel_in		(core_1_channel_receive),
   			.receive_flow_ctrl_in		(core_1_flow_ctrl_receive),

			//VC1
			.router2proc_VC1_cmd_out	(core_1_router2proc_VC1_cmd_out),
			.router2proc_VC1_addr_out	(core_1_router2proc_VC1_addr_out),
			.router2proc_VC1_req_out	(core_1_router2proc_VC1_req_out),
			//VC2		
			.router2proc_VC2_cmd_out	(core_1_router2proc_VC2_cmd_out),
			.router2proc_VC2_addr_out	(core_1_router2proc_VC2_addr_out),
			.router2proc_VC2_data_out	(core_1_router2proc_VC2_data_out),
			.router2proc_VC2_ack_out	(core_1_router2proc_VC2_ack_out)
		);

	// router connected to core 2
	core_interface network_interface_2 (
 	
   			.clk				(clk),
   			.reset				(reset),
			.error				(rtr_network_error[2]),

			.core_req_in			(core_2_req_in),
			.src_rtr_addr_in		(2'b01),		
   			.dest_rtr_addr_in		(core_2_dest_rtr_addr_in),
			.req_rtr_addr_in		(core_2_req_rtr_addr_in),
			.ack_count_in			(core_2_ack_count_in),
			.cache_addr_in			(core_2_addr_in),
			.cache_data_in			(core_2_data_in),
   	
   			.send_channel_out		(core_2_channel_send),
			.send_flow_ctrl_out		(core_2_flow_ctrl_send),
 		
 			.receive_channel_in		(core_2_channel_receive),
   			.receive_flow_ctrl_in		(core_2_flow_ctrl_receive),

			//VC1
			.router2proc_VC1_cmd_out	(core_2_router2proc_VC1_cmd_out),
			.router2proc_VC1_addr_out	(core_2_router2proc_VC1_addr_out),
			.router2proc_VC1_req_out	(core_2_router2proc_VC1_req_out),
			//VC2		
			.router2proc_VC2_cmd_out	(core_2_router2proc_VC2_cmd_out),
			.router2proc_VC2_addr_out	(core_2_router2proc_VC2_addr_out),
			.router2proc_VC2_data_out	(core_2_router2proc_VC2_data_out),
			.router2proc_VC2_ack_out	(core_2_router2proc_VC2_ack_out)
		);

	// router connected to directory
	directory_interface network_interface_3 (
 	
   			.clk				(clk),
   			.reset				(reset),
			.error				(rtr_network_error[3]),

			.core_req_in			(directory_req_in),
			.src_rtr_addr_in		(2'b11),		
   			.dest_rtr_addr_in		(directory_dest_rtr_addr_in),
			.req_rtr_addr_in		(directory_req_rtr_addr_in),
			.ack_count_in			(directory_ack_count_in),
			.cache_addr_in			(directory_addr_in),
			.cache_data_in			(directory_data_in),
   
   			.send_channel_out		(directory_channel_send),
			.send_flow_ctrl_out		(directory_flow_ctrl_send),
 			
 			.receive_channel_in		(directory_channel_receive),
   			.receive_flow_ctrl_in		(directory_flow_ctrl_receive),
			
			//VC0
			.router2dir_VC0_cmd_out		(directory_router2dir_VC0_cmd_out),
			.router2dir_VC0_addr_out	(directory_router2dir_VC0_addr_out),
			.router2dir_VC0_req_out		(directory_router2dir_VC0_req_out),		
			.router2dir_VC0_data_out	(directory_router2dir_VC0_data_out),

			.router2dir_VC2_cmd_out		(directory_router2dir_VC2_cmd_out),
			.router2dir_VC2_addr_out	(directory_router2dir_VC2_addr_out),
			.router2dir_VC2_req_out		(directory_router2dir_VC2_req_out),		
			.router2dir_VC2_data_out	(directory_router2dir_VC2_data_out)
		);

	// 2x2 mesh router network
	router_network router_network_0 (
			.clk				(clk),
			.reset				(reset),	
			.channel_router_0_inj		(core_0_channel_send),
			.channel_router_1_inj		(core_1_channel_send),
			.channel_router_2_inj		(core_2_channel_send),
			.channel_router_3_inj		(directory_channel_send),
			.flow_ctrl_router_0_inj		(core_0_flow_ctrl_send),
			.flow_ctrl_router_1_inj		(core_1_flow_ctrl_send),
			.flow_ctrl_router_2_inj		(core_2_flow_ctrl_send),
			.flow_ctrl_router_3_inj		(directory_flow_ctrl_send),

			.channel_router_0_eje		(core_0_channel_receive),
			.channel_router_1_eje		(core_1_channel_receive),
			.channel_router_2_eje		(core_2_channel_receive),
			.channel_router_3_eje		(directory_channel_receive),
			.flow_ctrl_router_0_eje		(core_0_flow_ctrl_receive),
			.flow_ctrl_router_1_eje		(core_1_flow_ctrl_receive),
			.flow_ctrl_router_2_eje		(core_2_flow_ctrl_receive),
			.flow_ctrl_router_3_eje		(directory_flow_ctrl_receive),

			//Debug
			.channel_router_0_op_0		(channel_router_0_op_0),
			.channel_router_0_op_1		(channel_router_0_op_1),
			.channel_router_0_op_2		(channel_router_0_op_2),
			.channel_router_0_op_3		(channel_router_0_op_3),
			.channel_router_1_op_3		(channel_router_1_op_3)
		);
endmodule

