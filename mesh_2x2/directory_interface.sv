/*  // maximum number of packets to generate (-1 = no limit)
   	localparam 	max_packet_count = 1000;
		
   	// width of packet count register
   	localparam 	packet_count_reg_width = 32;

   	// select packet length mode (0: uniform random, 1: bimodal)
   	localparam 	packet_length_mode = 0;
         
   
   	// width required to select individual resource class
   	localparam	resource_class_idx_width = clogb(num_resource_classes);
   
   	// total number of packet classes
   	localparam 	num_packet_classes = num_message_classes * num_resource_classes;
   
   	
   
   	// number of VCs
   	localparam 	num_vcs = num_packet_classes * num_vcs_per_class;
   
   	// width required to select individual VC
   	localparam 	vc_idx_width = clogb(num_vcs);
   

   	// total number of routers
   	localparam 	num_routers
   		  		= (num_nodes + num_nodes_per_router - 1) / num_nodes_per_router;
   
   	// number of routers in each dimension
   	localparam 	num_routers_per_dim = croot(num_routers, num_dimensions);
   
   	// width required to select individual router in a dimension
   	localparam 	dim_addr_width = clogb(num_routers_per_dim);
   
   	// width required to select individual router in entire network
   	localparam 	router_addr_width = num_dimensions * dim_addr_width;
   
   	// connectivity within each dimension
   	localparam 	connectivity = `CONNECTIVITY_LINE;
     				
   
   	// number of adjacent routers in each dimension
   	localparam 	num_neighbors_per_dim = 2;
   
   	// number of input and output ports on router
   	localparam 	num_ports = num_dimensions * num_neighbors_per_dim + num_nodes_per_router;
   
   	// width required to select individual port
   	localparam 	port_idx_width = clogb(num_ports);
   
   	// width required to select individual node at current router
   	localparam 	node_addr_width = clogb(num_nodes_per_router);
   
   	// width of global addresses
   	localparam 	addr_width = router_addr_width + node_addr_width;
   
   
   	// width of flow control signals
   	localparam 	flow_ctrl_width = 15;
   
   	
   
   	// number of bits required to represent all possible payload sizes
   	localparam 	payload_length_width
     				= clogb(max_payload_length-min_payload_length+1);
      
   	// width of link management signals
   	localparam 	link_ctrl_width = enable_link_pm ? 1 : 0;
   
   	// width of flit control signals
   	localparam 	flit_ctrl_width = 1 + vc_idx_width + 1 + 1;
   
   	
   	// channel width
   	localparam 	channel_width
     				= link_ctrl_width + flit_ctrl_width + flit_data_width;
   
   	// width required for lookahead routing information
   	localparam 	lar_info_width = port_idx_width + resource_class_idx_width;
   
   	// total number of bits required for storing routing information
   	localparam 	dest_info_width
   		  		= (routing_type == `ROUTING_TYPE_PHASED_DOR) ? 
     			 	  (num_resource_classes * router_addr_width + node_addr_width) : 
     				  -1;
   
   	// total number of bits required for routing-related information
   	localparam 	route_info_width = lar_info_width + dest_info_width;
   
   	// total number of bits required for storing header information
   	localparam 	header_info_width
   		  		= (packet_format == `PACKET_FORMAT_HEAD_TAIL) ? 
      				  route_info_width : 
     				  (packet_format == `PACKET_FORMAT_TAIL_ONLY) ? 
     				  route_info_width : 
     				  (packet_format == `PACKET_FORMAT_EXPLICIT_LENGTH) ? 
     				  (route_info_width + payload_length_width) : 
     				  -1;
     
   	// which router port is this packet source attached to?
   	localparam 	port_id = 4;
   
   	// which dimension does the current input port belong to?
   	localparam	curr_dim = port_id / num_neighbors_per_dim;
   
   	// maximum packet length (in flits)
   	localparam 	max_packet_length = 1 + max_payload_length;
   
   	// total number of bits required to represent maximum packet length
   	localparam 	packet_length_width = clogb(max_packet_length);

*/
   
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	module 	directory_interface (

		////////// Global Contrl Signal ////////////////////   	
   		input					clk,
   		input 					reset,
		output	logic				error,

		////////// Core -> Router Send Packet //////////////
	
		input	[0:3]				core_req_in,
		input	[0:1]				src_rtr_addr_in,		
   		input	[0:1]				dest_rtr_addr_in,
		input	[0:1]				req_rtr_addr_in,
		input	[0:1]				ack_count_in,
		input	[0:31]				cache_addr_in,
		input	[0:63]				cache_data_in,
   
   		output 	logic	[0:channel_width-1] 	send_channel_out,
		output	logic	[0:flow_ctrl_width-1]	send_flow_ctrl_out,
 
		
 		///////// Router -> Core Receive Packet ////////////
	
   		input 	[0:channel_width-1] 		receive_channel_in,
   		input 	[0:flow_ctrl_width-1] 		receive_flow_ctrl_in,


		output	logic	[0:3]			router2dir_VC0_cmd_out,
		output	logic	[0:31]			router2dir_VC0_addr_out,
		output	logic	[0:1]			router2dir_VC0_req_out,		
		output	logic	[0:63]			router2dir_VC0_data_out,

		output	logic	[0:3]			router2dir_VC2_cmd_out,
		output	logic	[0:31]			router2dir_VC2_addr_out,
		output	logic	[0:1]			router2dir_VC2_req_out,		
		output	logic	[0:63]			router2dir_VC2_data_out
		
		//output	logic				valid_packet_out,
		//output	logic	[0:3]			core_req_out,
		//output	logic	[0:31]			cache_addr_out,
		//output	logic	[0:63]			cache_data_out,	
		//output	logic	[0:1]			packet_vc_idx_out,
		//output	logic	[0:1]			src_rtr_addr_out,
		//output	logic	[0:1]			req_rtr_addr_out,		
		//output	logic	[0:1]			ack_cnt_out
	);
	       
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// signal for core -> router interface ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		logic					flit_kill;

		logic					valid_send_cmd;   
   		logic 					send_flit_head;
   		logic 					send_flit_tail;
   			
		logic					send_flit_valid;
		logic					middle_send_active;
		logic					head_send_active;
		logic					tail_send_active;
		
		logic	[0:63]				middle_send_data;	
		logic	[0:63]				tail_send_data;
		logic 	[0:31]				send_flit_data;
		logic	[0:flit_data_width-1]		flit_data;

		//logic	[0:1]				send_dest;
		logic	[0:1]				send_flit_dest;

		//logic	[0:1]				send_src;
		logic	[0:1]				send_flit_src;

		//logic	[0:1]				send_req_rtr;
		logic	[0:1]				send_flit_req_rtr;		
				
		//logic	[0:1]				send_ack;
		logic	[0:1]				send_flit_ack;

		//logic	[0:3]				sel_ovc;
		//logic	[0:3]				send_ovc;
		logic	[0:3]				send_flit_ovc;
			
		//logic	[0:3]				send_cmd;
		logic	[0:3]				send_flit_cmd;
		
		assign 	valid_send_cmd		= (core_req_in == 4'b0110) || (core_req_in == 4'b0111) || (core_req_in == 4'b1000) || 
						  (core_req_in == 4'b1001) || (core_req_in == 4'b1010) || (core_req_in == 4'b1011);

		assign 	flit_kill 		= (src_rtr_addr_in == dest_rtr_addr_in) && valid_send_cmd;
			
		assign	head_send_active	= (valid_send_cmd) && (!flit_kill) && (core_req_in == 4'b1010);

		assign	send_flit_valid 	= (valid_send_cmd && (!flit_kill)) || middle_send_active || tail_send_active;
					
		assign	send_flit_data 		= (valid_send_cmd)				? cache_addr_in :
						  (middle_send_active)  			? middle_send_data[0:31] : 
						  (tail_send_active)				? tail_send_data[32:63]  : 32'h0;

		assign	send_flit_cmd		= (valid_send_cmd)     				? core_req_in : 4'b0000;

		assign	send_flit_dest 		= (valid_send_cmd)     				? dest_rtr_addr_in : 2'b00;

		assign	send_flit_src 		= (valid_send_cmd)     				? src_rtr_addr_in : 2'b00;

		assign	send_flit_ack 		= (valid_send_cmd)     				? ack_count_in : 2'b00;

		assign	send_flit_req_rtr 	= (valid_send_cmd)     				? req_rtr_addr_in : 2'b00;

		assign 	send_flit_head 		= valid_send_cmd;
		assign	send_flit_tail 		= tail_send_active || ((valid_send_cmd) && (!flit_kill) && (core_req_in != 4'b1010)); 

		assign	send_flit_ovc		= ((core_req_in == 4'b1010) || (core_req_in == 4'b1011)) && (valid_send_cmd) ? 4'b0010 : 4'b0100;

		always @(posedge clk) begin
			if(reset) begin					
				middle_send_active	<= 1'b0;
				tail_send_active	<= 1'b0;
				middle_send_data	<= 64'h0;
				tail_send_data		<= 64'h0;	
			end
			else begin					
				middle_send_active	<= head_send_active;
				tail_send_active	<= middle_send_active;
				middle_send_data	<= cache_data_in;
				tail_send_data		<= middle_send_data;					
			end
		end
			
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// head info //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		logic 	[0:port_idx_width-1] 				route_port;
		logic 	[0:header_info_width-1]				header_info;
		logic	[0:dest_info_width-1] 				dest_info;
		logic 	[0:lar_info_width-1] 				lar_info;

		logic 	[0:num_ports-1] 				route_op;
   		logic 	[0:num_resource_classes-1] 			route_orc;

		logic 	[0:num_message_classes*num_resource_classes-1] 	sel_mc_orc;	
   		c_mat_mult
     		#(	.dim1_width(num_message_classes*num_resource_classes),
       			.dim2_width(num_vcs_per_class),
       			.dim3_width(1),
       			.prod_op(`BINARY_OP_AND),
       			.sum_op(`BINARY_OP_OR))
   		sel_mc_orc_mmult
     		(.input_a(send_flit_ovc),
      		 .input_b({num_vcs_per_class{1'b1}}),
      		.result(sel_mc_orc));
   
  	 	logic 	[0:num_message_classes-1] 		       	sel_mc;
	   	c_mat_mult
     		#(	.dim1_width(num_message_classes),
       			.dim2_width(num_resource_classes),
       			.dim3_width(1),
       			.prod_op(`BINARY_OP_AND),
       			.sum_op(`BINARY_OP_OR))
   		sel_mc_mmult
     		(	.input_a(sel_mc_orc),
      			.input_b({num_resource_classes{1'b1}}),
      			.result(sel_mc));
   
   		logic 	[0:num_resource_classes*num_message_classes-1] 	sel_orc_mc;
   		c_interleave
     		#(	.width(num_message_classes*num_resource_classes),
       			.num_blocks(num_message_classes))
   		sel_orc_mc_intl
     		(	.data_in(sel_mc_orc),
      			.data_out(sel_orc_mc));
   
   		logic 	[0:num_resource_classes-1] 		       	sel_orc;
   		c_mat_mult
     		#(	.dim1_width(num_resource_classes),
       			.dim2_width(num_message_classes),
       			.dim3_width(1),
       			.prod_op(`BINARY_OP_AND),
       			.sum_op(`BINARY_OP_OR))
   		sel_orc_mmult
     		(	.input_a(sel_orc_mc),
      			.input_b({num_message_classes{1'b1}}),
      			.result(sel_orc));
   		
		rtr_routing_logic
     		#(	.num_message_classes(num_message_classes),
       			.num_resource_classes(num_resource_classes),
       			.num_routers_per_dim(num_routers_per_dim),
       			.num_dimensions(num_dimensions),
       			.num_nodes_per_router(num_nodes_per_router),
       			.connectivity(connectivity),
       			.routing_type(routing_type),
       			.dim_order(dim_order))
   		rtl
     		(	.router_address(src_rtr_addr_in),
      			.sel_mc(sel_mc),
      			.sel_irc(sel_orc),
      			.dest_info(send_flit_dest),
      			.route_op(route_op),
      			.route_orc(route_orc));		

		c_encode
     		#(	.num_ports(num_ports))
   		route_port_enc
     		(	.data_in(route_op),
      			.data_out(route_port));

		logic 	[0:num_resource_classes*router_addr_width-1] 	dest_info_addresses;
   		assign 	dest_info_addresses = send_flit_dest;
   
   		logic 	[0:router_addr_width-1] 		     	rc_dest;
   		c_select_1ofn
     		#(	.num_ports(num_resource_classes),
       			.width(router_addr_width))
   		rc_dest_sel
     		(	.select(send_flit_orc),
      			.data_in(dest_info_addresses),
      			.data_out(rc_dest));   		
   		   
   		assign 	lar_info[0:port_idx_width-1] = route_port;

		assign 	header_info[0:lar_info_width-1] = lar_info;
		assign 	header_info[lar_info_width:route_info_width-1] = send_flit_dest;
   
   		assign 	flit_data[0:header_info_width-1] = header_info;
   		assign 	flit_data[header_info_width:flit_data_width-1] = {send_flit_cmd,send_flit_req_rtr,send_flit_src,send_flit_ack,{(32-header_info_width-10){1'b0}},send_flit_data};


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// encode packet //////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		rtr_channel_output
     		#(	.num_vcs		(num_vcs),
       			.packet_format		(packet_format),
       			.enable_link_pm		(enable_link_pm),
       			.flit_data_width	(flit_data_width),
       			.reset_type		(reset_type))
   		cho
     		(	.clk			(clk),
      			.reset			(reset),
			.active			(1'b1),
      			.flit_valid_in		(send_flit_valid),
      			.flit_head_in		(send_flit_head),
      			.flit_tail_in		(send_flit_tail),			
      			.flit_data_in		(flit_data),
      			.flit_sel_in_ovc	(send_flit_ovc),
      			.channel_out		(send_channel_out));


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// signal for router -> core interface ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//past outputs
		logic				valid_packet_out;
		logic	[0:3]			core_req_out;
		logic	[0:31]			cache_addr_out;
		logic	[0:63]			cache_data_out;	
		logic	[0:1]			packet_vc_idx_out;
		logic	[0:1]			src_rtr_addr_out;
		logic	[0:1]			req_rtr_addr_out;		
		logic	[0:1]			ack_cnt_out;

		//flit process
		logic	[0:64]			receive_flit_body;		
		logic	[0:31]			receive_flit_data;				
		logic	[0:flit_ctrl_width-1]	receive_flit_ctrl;		
		logic				receive_flit_head;
		logic				receive_flit_tail;
		logic	[0:1]			receive_flit_vc_idx;
		logic	[0:1]			receive_flit_src_rtr;
		logic	[0:1]			receive_flit_req_rtr;
		logic	[0:1]			receive_flit_ack_cnt;				
		logic				receive_flit_valid;
				
		logic	[0:3]			receive_core_req;

		logic				receive_flit_middle;
		
		assign	receive_flit_body 	= receive_channel_in[flit_ctrl_width:channel_width-1];
		assign 	receive_flit_valid 	= receive_flit_ctrl[0];
		assign	receive_flit_data	= receive_channel_in[channel_width-32:channel_width-1];
		assign	receive_flit_ctrl 	= receive_channel_in[0:flit_ctrl_width-1];	
		assign	receive_flit_vc_idx	= receive_flit_ctrl[1:1+vc_idx_width-1];
	
		assign	receive_core_req	= receive_flit_body[header_info_width:header_info_width+3];
		
		assign	receive_flit_src_rtr	= receive_flit_body[header_info_width+6:header_info_width+7];
		assign	receive_flit_req_rtr	= receive_flit_body[header_info_width+4:header_info_width+5];	
		assign	receive_flit_ack_cnt	= receive_flit_body[header_info_width+8:header_info_width+9];	
		
		assign	receive_flit_head 	= receive_flit_valid && receive_flit_ctrl[1+vc_idx_width+0];
		assign	receive_flit_tail 	= receive_flit_valid && receive_flit_ctrl[1+vc_idx_width+1];
		

		always @(posedge clk) begin
			if(reset) begin
				core_req_out		<= 4'b0000;
				cache_addr_out		<= 32'h0;
				cache_data_out		<= 64'h0;
				valid_packet_out	<= 1'b0;
				packet_vc_idx_out	<= 2'b00;
				src_rtr_addr_out	<= 2'b00;
				req_rtr_addr_out	<= 2'b00;			
				ack_cnt_out		<= 2'b00;
				receive_flit_middle	<= 1'b0;
			end
			else begin					
				core_req_out		<= receive_core_req;
				valid_packet_out	<= receive_flit_tail;
				packet_vc_idx_out	<= receive_flit_vc_idx;
				src_rtr_addr_out	<= receive_flit_src_rtr;
				req_rtr_addr_out	<= receive_flit_req_rtr;		
				ack_cnt_out		<= receive_flit_ack_cnt;
				receive_flit_middle	<= receive_flit_head;
					
				if(receive_flit_head)
					cache_addr_out		<= receive_flit_data;
				if(receive_flit_middle)
					cache_data_out[0:31]	<= receive_flit_data;				
				if(receive_flit_tail) 
					cache_data_out[32:63]	<= receive_flit_data;											
			end					
		end 

		// VC0
		assign 	router2dir_VC0_cmd_out		= valid_packet_out && (packet_vc_idx_out == 2'b00) ? core_req_out : 4'b0000; 
		assign 	router2dir_VC0_addr_out		= cache_addr_out;	
		assign	router2dir_VC0_req_out		= req_rtr_addr_out;
		assign	router2dir_VC0_data_out		= cache_data_out[0:63];

		assign 	router2dir_VC2_cmd_out		= valid_packet_out && (packet_vc_idx_out == 2'b10) ? core_req_out : 4'b0000; 
		assign 	router2dir_VC2_addr_out		= cache_addr_out;	
		assign	router2dir_VC2_req_out		= req_rtr_addr_out;
		assign	router2dir_VC2_data_out		= cache_data_out[0:63];
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////// flow control state tracker ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   		logic 			fc_event_valid;
   		logic 	[0:num_vcs-1] 	fc_event_sel_ovc;
   		rtr_flow_ctrl_input
     		#(	.num_vcs(num_vcs),
       			.flow_ctrl_type(flow_ctrl_type),
       			.reset_type(reset_type))
   		fci
     		(	.clk(clk),
      			.reset(reset),
      			.active(1'b1),
      			.flow_ctrl_in(receive_flow_ctrl_in),
      			.fc_event_valid_out(fc_event_valid),
      			.fc_event_sel_out_ovc(fc_event_sel_ovc));

   
   		logic 	flit_valid_s, flit_valid_q;
   		assign 	flit_valid_s = send_flit_valid;
   		c_dff
     		#(	.width(1),
       			.reset_type(reset_type))
   		flit_validq
     		(	.clk(clk),
      			.reset(reset),
      			.active(1'b1),
      			.d(flit_valid_s),
      			.q(flit_valid_q));
   
   		logic 	flit_head_s, flit_head_q;
   		assign 	flit_head_s = send_flit_head;
   		c_dff
     		#(	.width(1),
       			.reset_type(reset_type))
   		flit_headq
     		(	.clk(clk),
      			.reset(reset),
      			.active(1'b1),
      			.d(flit_head_s),
      			.q(flit_head_q));
   
   		logic 	flit_tail_s, flit_tail_q;
   		assign 	flit_tail_s = send_flit_tail;
   		c_dff
     		#(	.width(1),
       			.reset_type(reset_type))
   		flit_tailq
     		(	.clk(clk),
      			.reset(reset),
      			.active(1'b1),
      			.d(flit_tail_s),
      			.q(flit_tail_q));
   
  		logic 	[0:num_vcs-1] 	flit_sel_ovc_s, flit_sel_ovc_q;
   		assign 	flit_sel_ovc_s = send_flit_ovc;
   		c_dff
     		#(	.width(num_vcs),
       			.reset_type(reset_type))
   		flit_sel_ovcq
     		(	.clk(clk),
      			.reset(reset),
      			.active(1'b1),
      			.d(flit_sel_ovc_s),
      			.q(flit_sel_ovc_q));
   
   		logic 				fc_active;
   		logic 	[0:num_vcs-1] 		empty_ovc;
   		logic 	[0:num_vcs-1] 		almost_full_ovc;
   		logic 	[0:num_vcs-1] 		full_ovc;
   		logic 	[0:num_vcs-1] 		full_prev_ovc;
   		logic 	[0:num_vcs*2-1] 	fcs_errors_ovc;
   
		rtr_fc_state
     		#(	.num_vcs(num_vcs),
       			.buffer_size(buffer_size),
       			.flow_ctrl_type(flow_ctrl_type),
       			.flow_ctrl_bypass(flow_ctrl_bypass),
       			.mgmt_type(fb_mgmt_type),
       			.disable_static_reservations(disable_static_reservations),
       			.reset_type(reset_type))
   		fcs
     		(	.clk(clk),
      			.reset(reset),
      			.active(1'b1),
      			.flit_valid(flit_valid_q),
      			.flit_head(flit_head_q),
      			.flit_tail(flit_tail_q),
      			.flit_sel_ovc(flit_sel_ovc_q),
      			.fc_event_valid(fc_event_valid),
     		 	.fc_event_sel_ovc(fc_event_sel_ovc),
      			.fc_active(fc_active),
      			.empty_ovc(empty_ovc),
      			.almost_full_ovc(almost_full_ovc),
      			.full_ovc(full_ovc),
      			.full_prev_ovc(full_prev_ovc),
      			.errors_ovc(fcs_errors_ovc));
   
   		assign 	error = |fcs_errors_ovc;
   
endmodule

