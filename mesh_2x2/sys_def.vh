
typedef enum logic [3:0] {
	CMD_IDLE	= 4'b0000,
  	GetS        	= 4'b0001,
  	GetM		= 4'b0010,
  	PutS		= 4'b0011,
  	PutM		= 4'b0100,
	PutE		= 4'b0101,
  	Fwd_GetS	= 4'b0110,
  	Fwd_GetM	= 4'b0111,
	Invalid		= 4'b1000,
	Put_Ack		= 4'b1001,
	Data		= 4'b1010,
	Inv_Ack		= 4'b1011
} INTERFACE_CMD



