     
// channel latency in cycles
parameter 	channel_latency = 1;

// only inject traffic at the node ports
parameter 	inject_node_ports_only = 1;

/*
      
// select packet length mode (0: uniform random, 1: bimodal)
//parameter 	packet_length_mode = 0;
    

// width required to select individual resource class
//localparam	resource_class_idx_width = clogb(num_resource_classes);
   
// total number of packet classes
//localparam 	num_packet_classes = num_message_classes * num_resource_classes;
   
// number of VCs
//localparam 	num_vcs = num_packet_classes * num_vcs_per_class;
   
// width required to select individual VC
//localparam 	vc_idx_width = clogb(num_vcs);
   
// total number of routers
//localparam 	num_routers
//  	 		= (num_nodes + num_nodes_per_router - 1) / num_nodes_per_router;
   
// number of routers in each dimension
localparam 	num_routers_per_dim = croot(num_routers, num_dimensions);
   
// width required to select individual router in a dimension
localparam 	dim_addr_width = clogb(num_routers_per_dim);
   
// width required to select individual router in entire network
localparam 	router_addr_width = num_dimensions * dim_addr_width;
   
// connectivity within each dimension
localparam 	connectivity
    		 		= (topology == `TOPOLOGY_MESH) ?
       				  `CONNECTIVITY_LINE :
       				  (topology == `TOPOLOGY_TORUS) ?
      	 			  `CONNECTIVITY_RING :
       				  (topology == `TOPOLOGY_FBFLY) ?
        			  `CONNECTIVITY_FULL :
       				  -1;
   
// number of adjacent routers in each dimension
localparam 	num_neighbors_per_dim
     				= ((connectivity == `CONNECTIVITY_LINE) ||
			  	  (connectivity == `CONNECTIVITY_RING)) ?
       			          2 :
       			  	  (connectivity == `CONNECTIVITY_FULL) ?
       			          (num_routers_per_dim - 1) :
                          	  -1;
   
// number of input and output ports on router
localparam 	num_ports
     				= num_dimensions * num_neighbors_per_dim + num_nodes_per_router;
   
// width required to select individual port
localparam 	port_idx_width = clogb(num_ports);
   
// width required to select individual node at current router
localparam 	node_addr_width = clogb(num_nodes_per_router);
   
// width required for lookahead routing information
localparam 	lar_info_width = port_idx_width + resource_class_idx_width;
   	
// total number of bits required for storing routing information
localparam 	dest_info_width
     				= (routing_type == `ROUTING_TYPE_PHASED_DOR) ? 
       			  	  (num_resource_classes * router_addr_width + node_addr_width) : 
        		   	  -1;
   
// total number of bits required for routing-related information
localparam 	route_info_width = lar_info_width + dest_info_width;
   
// width of flow control signals
localparam 	flow_ctrl_width
   	 		 	= (flow_ctrl_type == `FLOW_CTRL_TYPE_CREDIT) ? (1 + vc_idx_width) :
       			   	  -1;
   
// width of link management signals
localparam 	link_ctrl_width = enable_link_pm ? 1 : 0;
   
// width of flit control signals
localparam 	flit_ctrl_width
     				= (packet_format == `PACKET_FORMAT_HEAD_TAIL) ? 
       			  	  (1 + vc_idx_width + 1 + 1) : 
       			  	  (packet_format == `PACKET_FORMAT_TAIL_ONLY) ? 
       			 	  (1 + vc_idx_width + 1) : 
       				  (packet_format == `PACKET_FORMAT_EXPLICIT_LENGTH) ? 
       				  (1 + vc_idx_width + 1) : 
       				  -1;
   
// channel width
localparam 	channel_width
     		  		= link_ctrl_width + flit_ctrl_width + flit_data_width;
   
// use atomic VC allocation
localparam 	atomic_vc_allocation = (elig_mask == `ELIG_MASK_USED);
   
// number of pipeline stages in the channels
localparam 	num_channel_stages = channel_latency - 1;
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////// 2x2 mesh network //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

module router_network (
	input				clk,
	input				reset,	
	input	[0:channel_width-1]	channel_router_0_inj,
	input	[0:channel_width-1]	channel_router_1_inj,
	input	[0:channel_width-1]	channel_router_2_inj,
	input	[0:channel_width-1]	channel_router_3_inj,
	input	[0:flow_ctrl_width-1]	flow_ctrl_router_0_inj,
	input	[0:flow_ctrl_width-1]	flow_ctrl_router_1_inj,
	input	[0:flow_ctrl_width-1]	flow_ctrl_router_2_inj,
	input	[0:flow_ctrl_width-1]	flow_ctrl_router_3_inj,

	output	logic	[0:channel_width-1]	channel_router_0_eje,
	output	logic	[0:channel_width-1]	channel_router_1_eje,
	output	logic	[0:channel_width-1]	channel_router_2_eje,
	output	logic	[0:channel_width-1]	channel_router_3_eje,
	output	logic	[0:flow_ctrl_width-1]	flow_ctrl_router_0_eje,
	output	logic	[0:flow_ctrl_width-1]	flow_ctrl_router_1_eje,
	output	logic	[0:flow_ctrl_width-1]	flow_ctrl_router_2_eje,
	output	logic	[0:flow_ctrl_width-1]	flow_ctrl_router_3_eje,

	//Debug
	output	logic 	[0:channel_width-1] 	channel_router_0_op_0,
	output	logic 	[0:channel_width-1] 	channel_router_0_op_1,
	output	logic 	[0:channel_width-1] 	channel_router_0_op_2,
	output	logic 	[0:channel_width-1] 	channel_router_0_op_3,
	output	logic 	[0:channel_width-1] 	channel_router_1_op_3

	);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////// 2x2 mesh network //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//logic 	[0:channel_width-1] 	channel_router_0_op_0;
	//logic 	[0:channel_width-1] 	channel_router_0_op_1;
	//logic 	[0:channel_width-1] 	channel_router_0_op_2;
	//logic 	[0:channel_width-1] 	channel_router_0_op_3;
	logic 	[0:channel_width-1] 	channel_router_0_op_4;
	logic 	[0:channel_width-1] 	channel_router_0_ip_0;
	logic 	[0:channel_width-1] 	channel_router_0_ip_1;
	logic 	[0:channel_width-1] 	channel_router_0_ip_2;
	logic 	[0:channel_width-1] 	channel_router_0_ip_3;
	logic 	[0:channel_width-1] 	channel_router_0_ip_4;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_0_ip_0;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_0_ip_1;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_0_ip_2;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_0_ip_3;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_0_ip_4;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_0_op_0;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_0_op_1;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_0_op_2;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_0_op_3;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_0_op_4;

	logic 	[0:channel_width-1] 	channel_router_1_op_0;
	logic 	[0:channel_width-1] 	channel_router_1_op_1;
	logic 	[0:channel_width-1] 	channel_router_1_op_2;
	//logic 	[0:channel_width-1] 	channel_router_1_op_3;
	logic 	[0:channel_width-1] 	channel_router_1_op_4;
	logic 	[0:channel_width-1] 	channel_router_1_ip_0;
	logic 	[0:channel_width-1] 	channel_router_1_ip_1;
	logic 	[0:channel_width-1] 	channel_router_1_ip_2;
	logic 	[0:channel_width-1] 	channel_router_1_ip_3;
	logic 	[0:channel_width-1] 	channel_router_1_ip_4;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_1_ip_0;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_1_ip_1;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_1_ip_2;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_1_ip_3;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_1_ip_4;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_1_op_0;
	logic	[0:flow_ctrl_width-1] 		flow_ctrl_router_1_op_1;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_1_op_2;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_1_op_3;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_1_op_4;

	logic	[0:channel_width-1] 	channel_router_2_op_0;
	logic 	[0:channel_width-1] 	channel_router_2_op_1;
	logic 	[0:channel_width-1] 	channel_router_2_op_2;
	logic 	[0:channel_width-1] 	channel_router_2_op_3;
	logic 	[0:channel_width-1] 	channel_router_2_op_4;
	logic 	[0:channel_width-1] 	channel_router_2_ip_0;
	logic 	[0:channel_width-1] 	channel_router_2_ip_1;
	logic 	[0:channel_width-1] 	channel_router_2_ip_2;
	logic 	[0:channel_width-1] 	channel_router_2_ip_3;
	logic 	[0:channel_width-1] 	channel_router_2_ip_4;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_2_ip_0;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_2_ip_1;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_2_ip_2;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_2_ip_3;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_2_ip_4;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_2_op_0;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_2_op_1;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_2_op_2;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_2_op_3;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_2_op_4;

	logic 	[0:channel_width-1] 	channel_router_3_op_0;
	logic 	[0:channel_width-1] 	channel_router_3_op_1;
	logic 	[0:channel_width-1] 	channel_router_3_op_2;
	logic 	[0:channel_width-1] 	channel_router_3_op_3;
	logic 	[0:channel_width-1] 	channel_router_3_op_4;
	logic 	[0:channel_width-1] 	channel_router_3_ip_0;
	logic 	[0:channel_width-1] 	channel_router_3_ip_1;
	logic 	[0:channel_width-1] 	channel_router_3_ip_2;
	logic 	[0:channel_width-1] 	channel_router_3_ip_3;
	logic 	[0:channel_width-1] 	channel_router_3_ip_4;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_3_ip_0;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_3_ip_1;
	logic	[0:flow_ctrl_width-1] 		flow_ctrl_router_3_ip_2;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_3_ip_3;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_3_ip_4;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_3_op_0;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_3_op_1;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_3_op_2;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_3_op_3;
	logic 	[0:flow_ctrl_width-1] 		flow_ctrl_router_3_op_4;


	//wires that are connected to the flit_sink and packet_source modules
   	//logic 	[0:(num_routers*channel_width)-1] 	injection_channels;
   	//logic 	[0:(num_routers*flow_ctrl_width)-1] 	injection_flow_ctrl;
   	//logic 	[0:(num_routers*channel_width)-1] 	ejection_channels;
   	//logic 	[0:(num_routers*flow_ctrl_width)-1] 	ejection_flow_ctrl;  


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// connected together channels and flow_ctrl - 2x2 mesh network //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//assign	channel_router_0_eje	= channel_router_0_op_4;	
	//assign	channel_router_1_eje	= channel_router_1_op_4;
	//assign	channel_router_2_eje	= channel_router_2_op_4;
	//assign	channel_router_3_eje	= channel_router_3_op_4;

	// router_0 connection
   	assign 	channel_router_0_ip_0 	= {channel_width{1'b0}};
   	assign 	channel_router_0_ip_1 	= channel_router_1_op_0;
   	assign 	channel_router_0_ip_2 	= {channel_width{1'b0}};
   	assign 	channel_router_0_ip_3 	= channel_router_2_op_2;
   	//assign 	channel_router_0_ip_4 	= injection_channels[0*channel_width:(1*channel_width)-1];
	assign 	channel_router_0_ip_4	= channel_router_0_inj;

  	assign 	flow_ctrl_router_0_op_0 = {flow_ctrl_width{1'b0}};
   	assign 	flow_ctrl_router_0_op_1 = flow_ctrl_router_1_ip_0;
   	assign 	flow_ctrl_router_0_op_2 = {flow_ctrl_width{1'b0}};
   	assign 	flow_ctrl_router_0_op_3 = flow_ctrl_router_2_ip_2;
   	//assign 	flow_ctrl_router_0_op_4 = ejection_flow_ctrl[0*flow_ctrl_width:(1*flow_ctrl_width)-1];
	assign 	flow_ctrl_router_0_op_4	= flow_ctrl_router_0_inj;

	// router_1 connection
   	assign 	channel_router_1_ip_0 	= channel_router_0_op_1;
   	assign 	channel_router_1_ip_1 	= {channel_width{1'b0}};
   	assign 	channel_router_1_ip_2 	= {channel_width{1'b0}};
   	assign 	channel_router_1_ip_3 	= channel_router_3_op_2;
   	//assign 	channel_router_1_ip_4 	= injection_channels[1*channel_width:(2*channel_width)-1];
	assign 	channel_router_1_ip_4	= channel_router_1_inj;

   	assign 	flow_ctrl_router_1_op_0 = flow_ctrl_router_0_ip_1;
   	assign 	flow_ctrl_router_1_op_1 = {flow_ctrl_width{1'b0}};
   	assign	flow_ctrl_router_1_op_2 = {flow_ctrl_width{1'b0}};
   	assign 	flow_ctrl_router_1_op_3 = flow_ctrl_router_3_ip_2;
   	//assign 	flow_ctrl_router_1_op_4 = ejection_flow_ctrl[1*flow_ctrl_width:(2*flow_ctrl_width)-1];
	assign 	flow_ctrl_router_1_op_4	= flow_ctrl_router_1_inj;

	// router_2 connection
   	assign 	channel_router_2_ip_0 	= {channel_width{1'b0}};
   	assign 	channel_router_2_ip_1 	= channel_router_3_op_0;
   	assign 	channel_router_2_ip_2 	= channel_router_0_op_3;;
   	assign 	channel_router_2_ip_3 	= {channel_width{1'b0}};
   	//assign 	channel_router_2_ip_4 	= injection_channels[2*channel_width:(3*channel_width)-1];
	assign 	channel_router_2_ip_4	= channel_router_2_inj;

   	assign 	flow_ctrl_router_2_op_0 = {flow_ctrl_width{1'b0}};
   	assign 	flow_ctrl_router_2_op_1 = flow_ctrl_router_3_ip_0;
   	assign 	flow_ctrl_router_2_op_2 = flow_ctrl_router_0_ip_3;
   	assign 	flow_ctrl_router_2_op_3 = {flow_ctrl_width{1'b0}};
  	//assign 	flow_ctrl_router_2_op_4 = ejection_flow_ctrl[2*flow_ctrl_width:(3*flow_ctrl_width)-1];
	assign 	flow_ctrl_router_2_op_4	= flow_ctrl_router_2_inj;

	// router_3 connection
   	assign 	channel_router_3_ip_0 	= channel_router_2_op_1;
   	assign 	channel_router_3_ip_1 	= {channel_width{1'b0}};
   	assign 	channel_router_3_ip_2 	= channel_router_1_op_3;
   	assign 	channel_router_3_ip_3 	= {channel_width{1'b0}};
  	//assign 	channel_router_3_ip_4 	= injection_channels[3*channel_width:(4*channel_width)-1];
	assign 	channel_router_3_ip_4	= channel_router_3_inj;

   	assign 	flow_ctrl_router_3_op_0 = flow_ctrl_router_2_ip_1;
   	assign 	flow_ctrl_router_3_op_1 = {flow_ctrl_width{1'b0}};
   	assign 	flow_ctrl_router_3_op_2 = flow_ctrl_router_1_ip_3;
   	assign 	flow_ctrl_router_3_op_3 = {flow_ctrl_width{1'b0}};
   	//assign 	flow_ctrl_router_3_op_4 = ejection_flow_ctrl[3*flow_ctrl_width:(4*flow_ctrl_width)-1];
	assign 	flow_ctrl_router_3_op_4	= flow_ctrl_router_3_inj;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// connected routers to flit_sink and packet_source ///////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	assign 	flow_ctrl_router_0_eje 	= flow_ctrl_router_0_ip_4;

	assign 	channel_router_0_eje 	= channel_router_0_op_4;

	assign 	flow_ctrl_router_1_eje 	= flow_ctrl_router_1_ip_4;

	assign 	channel_router_1_eje	= channel_router_1_op_4;

	assign 	flow_ctrl_router_2_eje 	= flow_ctrl_router_2_ip_4;

	assign 	channel_router_2_eje	= channel_router_2_op_4;

	assign 	flow_ctrl_router_3_eje	= flow_ctrl_router_3_ip_4;

	assign 	channel_router_3_eje 	= channel_router_3_op_4;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// rounter instantiation /////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	logic 	[0 : num_routers-1]	rtr_error;

	router_wrap
     		#(	.topology(topology),
       			.buffer_size(buffer_size),
       			.num_message_classes(num_message_classes),
       			.num_resource_classes(num_resource_classes),
       			.num_vcs_per_class(num_vcs_per_class),
       			.num_nodes(num_nodes),
       			.num_dimensions(num_dimensions),
       			.num_nodes_per_router(num_nodes_per_router),
       			.packet_format(packet_format),
       			.flow_ctrl_type(flow_ctrl_type),
       			.flow_ctrl_bypass(flow_ctrl_bypass),
       			.max_payload_length(max_payload_length),
       			.min_payload_length(min_payload_length),
       			.router_type(router_type),
       			.enable_link_pm(enable_link_pm),
       			.flit_data_width(flit_data_width),
       			.error_capture_mode(error_capture_mode),
       			.restrict_turns(restrict_turns),
       			.predecode_lar_info(predecode_lar_info),
       			.routing_type(routing_type),
       			.dim_order(dim_order),
       			.input_stage_can_hold(input_stage_can_hold),
       			.fb_regfile_type(fb_regfile_type),
       			.fb_mgmt_type(fb_mgmt_type),
       			.explicit_pipeline_register(explicit_pipeline_register),
       			.dual_path_alloc(dual_path_alloc),
       			.dual_path_allow_conflicts(dual_path_allow_conflicts),
       			.dual_path_mask_on_ready(dual_path_mask_on_ready),
       			.precomp_ivc_sel(precomp_ivc_sel),
       			.precomp_ip_sel(precomp_ip_sel),
       			.elig_mask(elig_mask),
       			.vc_alloc_type(vc_alloc_type),
       			.vc_alloc_arbiter_type(vc_alloc_arbiter_type),
       			.vc_alloc_prefer_empty(vc_alloc_prefer_empty),
       			.sw_alloc_type(sw_alloc_type),
       			.sw_alloc_arbiter_type(sw_alloc_arbiter_type),
       			.sw_alloc_spec_type(sw_alloc_spec_type),
       			.crossbar_type(crossbar_type),
       			.reset_type(reset_type))
   	rtr_0
     		(	.clk(clk),
      			.reset(reset),
      			.router_address(2'b00),
      			.channel_in_ip({channel_router_0_ip_0, channel_router_0_ip_1, channel_router_0_ip_2, channel_router_0_ip_3, channel_router_0_ip_4}),
      			.flow_ctrl_out_ip({ flow_ctrl_router_0_ip_0, flow_ctrl_router_0_ip_1, flow_ctrl_router_0_ip_2, flow_ctrl_router_0_ip_3, flow_ctrl_router_0_ip_4 }),
      			.channel_out_op({ channel_router_0_op_0, channel_router_0_op_1, channel_router_0_op_2, channel_router_0_op_3, channel_router_0_op_4 }),
      			.flow_ctrl_in_op({ flow_ctrl_router_0_op_0, flow_ctrl_router_0_op_1, flow_ctrl_router_0_op_2, flow_ctrl_router_0_op_3, flow_ctrl_router_0_op_4 }),
      			.error(rtr_error[0]));
		


   	router_wrap
     		#(	.topology(topology),
       			.buffer_size(buffer_size),
       			.num_message_classes(num_message_classes),
       			.num_resource_classes(num_resource_classes),
       			.num_vcs_per_class(num_vcs_per_class),
       			.num_nodes(num_nodes),
       			.num_dimensions(num_dimensions),
       			.num_nodes_per_router(num_nodes_per_router),
       			.packet_format(packet_format),
       			.flow_ctrl_type(flow_ctrl_type),
       			.flow_ctrl_bypass(flow_ctrl_bypass),
       			.max_payload_length(max_payload_length),
       			.min_payload_length(min_payload_length),
       			.router_type(router_type),
       			.enable_link_pm(enable_link_pm),
       			.flit_data_width(flit_data_width),
       			.error_capture_mode(error_capture_mode),
       			.restrict_turns(restrict_turns),
       			.predecode_lar_info(predecode_lar_info),
       			.routing_type(routing_type),
       			.dim_order(dim_order),
      			.input_stage_can_hold(input_stage_can_hold),
       			.fb_regfile_type(fb_regfile_type),
       			.fb_mgmt_type(fb_mgmt_type),
       			.explicit_pipeline_register(explicit_pipeline_register),
       			.dual_path_alloc(dual_path_alloc),
       			.dual_path_allow_conflicts(dual_path_allow_conflicts),
       			.dual_path_mask_on_ready(dual_path_mask_on_ready),
       			.precomp_ivc_sel(precomp_ivc_sel),
       			.precomp_ip_sel(precomp_ip_sel),
       			.elig_mask(elig_mask),
       			.vc_alloc_type(vc_alloc_type),
       			.vc_alloc_arbiter_type(vc_alloc_arbiter_type),
       			.vc_alloc_prefer_empty(vc_alloc_prefer_empty),
       			.sw_alloc_type(sw_alloc_type),
       			.sw_alloc_arbiter_type(sw_alloc_arbiter_type),
       			.sw_alloc_spec_type(sw_alloc_spec_type),
      			.crossbar_type(crossbar_type),
       			.reset_type(reset_type))
   	rtr_1
     		(	.clk(clk),
      			.reset(reset),
      			.router_address(2'b10),
      			.channel_in_ip({channel_router_1_ip_0, channel_router_1_ip_1, channel_router_1_ip_2, channel_router_1_ip_3, channel_router_1_ip_4}),
      			.flow_ctrl_out_ip({ flow_ctrl_router_1_ip_0, flow_ctrl_router_1_ip_1, flow_ctrl_router_1_ip_2, flow_ctrl_router_1_ip_3, flow_ctrl_router_1_ip_4 }),
      			.channel_out_op({ channel_router_1_op_0, channel_router_1_op_1, channel_router_1_op_2, channel_router_1_op_3, channel_router_1_op_4 }),
      			.flow_ctrl_in_op({ flow_ctrl_router_1_op_0, flow_ctrl_router_1_op_1, flow_ctrl_router_1_op_2, flow_ctrl_router_1_op_3, flow_ctrl_router_1_op_4 }),
      			.error(rtr_error[1]));
		


   	router_wrap
     		#(	.topology(topology),
       			.buffer_size(buffer_size),
       			.num_message_classes(num_message_classes),
       			.num_resource_classes(num_resource_classes),
      			.num_vcs_per_class(num_vcs_per_class),
      			.num_nodes(num_nodes),
       			.num_dimensions(num_dimensions),
       			.num_nodes_per_router(num_nodes_per_router),
       			.packet_format(packet_format),
       			.flow_ctrl_type(flow_ctrl_type),
       			.flow_ctrl_bypass(flow_ctrl_bypass),
       			.max_payload_length(max_payload_length),
       			.min_payload_length(min_payload_length),
       			.router_type(router_type),
       			.enable_link_pm(enable_link_pm),
       			.flit_data_width(flit_data_width),
       			.error_capture_mode(error_capture_mode),
       			.restrict_turns(restrict_turns),
      			.predecode_lar_info(predecode_lar_info),
       			.routing_type(routing_type),
       			.dim_order(dim_order),
       			.input_stage_can_hold(input_stage_can_hold),
       			.fb_regfile_type(fb_regfile_type),
       			.fb_mgmt_type(fb_mgmt_type),
       			.explicit_pipeline_register(explicit_pipeline_register),
       			.dual_path_alloc(dual_path_alloc),
       			.dual_path_allow_conflicts(dual_path_allow_conflicts),
       			.dual_path_mask_on_ready(dual_path_mask_on_ready),
       			.precomp_ivc_sel(precomp_ivc_sel),
       			.precomp_ip_sel(precomp_ip_sel),
       			.elig_mask(elig_mask),
       			.vc_alloc_type(vc_alloc_type),
       			.vc_alloc_arbiter_type(vc_alloc_arbiter_type),
       			.vc_alloc_prefer_empty(vc_alloc_prefer_empty),
       			.sw_alloc_type(sw_alloc_type),
       			.sw_alloc_arbiter_type(sw_alloc_arbiter_type),
       			.sw_alloc_spec_type(sw_alloc_spec_type),
       			.crossbar_type(crossbar_type),
       			.reset_type(reset_type))
   	rtr_2
     		(	.clk(clk),
      			.reset(reset),
      			.router_address(2'b01),
      			.channel_in_ip({channel_router_2_ip_0, channel_router_2_ip_1, channel_router_2_ip_2, channel_router_2_ip_3, channel_router_2_ip_4}),
      			.flow_ctrl_out_ip({ flow_ctrl_router_2_ip_0, flow_ctrl_router_2_ip_1, flow_ctrl_router_2_ip_2, flow_ctrl_router_2_ip_3, flow_ctrl_router_2_ip_4 }),
      			.channel_out_op({ channel_router_2_op_0, channel_router_2_op_1, channel_router_2_op_2, channel_router_2_op_3, channel_router_2_op_4 }),
      			.flow_ctrl_in_op({ flow_ctrl_router_2_op_0, flow_ctrl_router_2_op_1, flow_ctrl_router_2_op_2, flow_ctrl_router_2_op_3, flow_ctrl_router_2_op_4 }),
      			.error(rtr_error[2]));
		


   	router_wrap
     		#(	.topology(topology),
       			.buffer_size(buffer_size),
       			.num_message_classes(num_message_classes),
       			.num_resource_classes(num_resource_classes),
      			.num_vcs_per_class(num_vcs_per_class),
      			.num_nodes(num_nodes),
       			.num_dimensions(num_dimensions),
       			.num_nodes_per_router(num_nodes_per_router),
       			.packet_format(packet_format),
       			.flow_ctrl_type(flow_ctrl_type),
       			.flow_ctrl_bypass(flow_ctrl_bypass),
       			.max_payload_length(max_payload_length),
       			.min_payload_length(min_payload_length),
       			.router_type(router_type),
       			.enable_link_pm(enable_link_pm),
       			.flit_data_width(flit_data_width),
       			.error_capture_mode(error_capture_mode),
       			.restrict_turns(restrict_turns),
       			.predecode_lar_info(predecode_lar_info),
       			.routing_type(routing_type),
       			.dim_order(dim_order),
       			.input_stage_can_hold(input_stage_can_hold),
       			.fb_regfile_type(fb_regfile_type),
       			.fb_mgmt_type(fb_mgmt_type),
       			.explicit_pipeline_register(explicit_pipeline_register),
       			.dual_path_alloc(dual_path_alloc),
       			.dual_path_allow_conflicts(dual_path_allow_conflicts),
       			.dual_path_mask_on_ready(dual_path_mask_on_ready),
       			.precomp_ivc_sel(precomp_ivc_sel),
       			.precomp_ip_sel(precomp_ip_sel),
       			.elig_mask(elig_mask),
       			.vc_alloc_type(vc_alloc_type),
       			.vc_alloc_arbiter_type(vc_alloc_arbiter_type),
       			.vc_alloc_prefer_empty(vc_alloc_prefer_empty),
       			.sw_alloc_type(sw_alloc_type),
       			.sw_alloc_arbiter_type(sw_alloc_arbiter_type),
       			.sw_alloc_spec_type(sw_alloc_spec_type),
       			.crossbar_type(crossbar_type),
       			.reset_type(reset_type))
   	rtr_3
     		(	.clk(clk),
      			.reset(reset),
      			.router_address(2'b11),
      			.channel_in_ip({channel_router_3_ip_0, channel_router_3_ip_1, channel_router_3_ip_2, channel_router_3_ip_3, channel_router_3_ip_4}),
      			.flow_ctrl_out_ip({ flow_ctrl_router_3_ip_0, flow_ctrl_router_3_ip_1, flow_ctrl_router_3_ip_2, flow_ctrl_router_3_ip_3, flow_ctrl_router_3_ip_4 }),
      			.channel_out_op({ channel_router_3_op_0, channel_router_3_op_1, channel_router_3_op_2, channel_router_3_op_3, channel_router_3_op_4 }),
      			.flow_ctrl_in_op({ flow_ctrl_router_3_op_0, flow_ctrl_router_3_op_1, flow_ctrl_router_3_op_2, flow_ctrl_router_3_op_3, flow_ctrl_router_3_op_4 }),
      			.error(rtr_error[3]));


endmodule
		
  
