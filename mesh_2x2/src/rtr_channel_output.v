//==============================================================================
// channel interface (send side)
//==============================================================================

module 	rtr_channel_output
  	(clk, reset, active, flit_valid_in, flit_head_in, flit_tail_in, flit_data_in, 
   	 flit_sel_in_ovc, channel_out);
   
`include "c_functions.v"
`include "c_constants.v"
`include "rtr_constants.v"

typedef enum logic [3:0] {
	CMD_IDLE	= 4'b0000,
  	GetS        	= 4'b0001,
  	GetM		= 4'b0010,
  	PutS		= 4'b0011,
  	PutM		= 4'b0100,
	PutE		= 4'b0101,
  	Fwd_GetS	= 4'b0110,
  	Fwd_GetM	= 4'b0111,
	Invalid		= 4'b1000,
	Put_Ack		= 4'b1001,
	Data		= 4'b1010,
	Inv_Ack		= 4'b1011
} INTERFACE_CMD;
   
   
   	//---------------------------------------------------------------------------
   	// parameters
   	//---------------------------------------------------------------------------
   	
   	// number of VCs
   	parameter	num_vcs = 4;
   
   	// select packet format
   	parameter 	packet_format = `PACKET_FORMAT_HEAD_TAIL;
   
   	// enable link power management
   	parameter 	enable_link_pm = 0;
   
   	// width of flit payload data
   	parameter 	flit_data_width = 64;
   
   	parameter 	reset_type = `RESET_TYPE_SYNC;
   
   
   	//---------------------------------------------------------------------------
   	// derived parameters
   	//---------------------------------------------------------------------------
   
   	// width required to select individual VC
   	localparam	vc_idx_width 	= clogb(num_vcs);
   
   	// width of link management signals
   	localparam	link_ctrl_width = enable_link_pm ? 1 : 0;
   
   	// width of flit control signals
   	localparam 	flit_ctrl_width	= 1 + vc_idx_width + 1 + 1; 
       	   
   	// channel width
   	localparam 	channel_width 	= link_ctrl_width + flit_ctrl_width + flit_data_width;
      
   	//---------------------------------------------------------------------------
   	// interface
   	//---------------------------------------------------------------------------
   
   	input 				clk;
   	input 				reset;
	input				active;
   
   	// flit valid indicator
   	input 				flit_valid_in;   
   	// flit is a head flit
   	input 				flit_head_in;   
   	// flit is a tail flit
   	input 				flit_tail_in;	
	   
   	// payload data
   	input	[0:flit_data_width-1] 	flit_data_in;
   
   	// indicate which VC the current flit (if any) belongs to
   	input 	[0:num_vcs-1] 	       	flit_sel_in_ovc;
   
   	// outgoing flit control signals
   	output 	[0:channel_width-1]  	channel_out;
   	wire 	[0:channel_width-1]    	channel_out;
   
   
   	//---------------------------------------------------------------------------
   	// implementation
   	//---------------------------------------------------------------------------
   
   	wire 	flit_valid_out;
   	      	
	assign 	channel_out[0] = flit_valid_out;
              
   	wire 	[0:flit_ctrl_width-1]  	flit_ctrl_out;
   	wire 	[0:flit_data_width-1]  	flit_data_out;
   
   	assign 	channel_out[0:channel_width-1] = {flit_ctrl_out,flit_data_out};
	   
   	wire 	[0:flit_data_width-1]  	flit_data_s, flit_data_q;

   	assign 	flit_data_s = flit_data_in;

   	c_dff
     	#(	.width(flit_data_width),
       		.reset_type(reset_type))
   	flit_dataq
     	(	.clk(clk),
      		.reset(reset),
      		.active(1'b1),
      		.d(flit_data_s),
      		.q(flit_data_q));
   
   	assign 	flit_data_out = flit_data_q;

   	generate
		wire [0:vc_idx_width-1] flit_vc;
		if(num_vcs > 1)
	       	begin		  
		  	//wire [0:vc_idx_width-1] flit_vc;
		  	c_encode
		    	#(	.num_ports(num_vcs))
		  	flit_vc_enc
		    	(	.data_in(flit_sel_in_ovc),
		    		.data_out(flit_vc));
		  		  
	     	end
	     
	  	wire 	[0:flit_ctrl_width-1] 	flit_ctrl_s, flit_ctrl_q;
	     
	     	assign 	flit_ctrl_s[0:flit_ctrl_width-1] = {flit_valid_in,flit_vc,flit_head_in,flit_tail_in};
			     	     	  
		c_dff
		#(	.width(flit_ctrl_width),
		      	.reset_type(reset_type))
		flit_ctrlq
		(	.clk(clk),
		     	.reset(reset),
		    	.active(1'b1),
		    	.d(flit_ctrl_s[0:flit_ctrl_width-1]),
		    	.q(flit_ctrl_q[0:flit_ctrl_width-1]));
	     
	     	assign 	flit_ctrl_out = flit_ctrl_q;
      
   endgenerate
   
endmodule
