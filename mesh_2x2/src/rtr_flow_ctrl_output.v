
//==============================================================================
// flow control interface (send side)
//==============================================================================

module rtr_flow_ctrl_output
       (clk, active, reset, fc_event_valid_in, fc_event_sel_in_ivc, flow_ctrl_out);
   
`include "c_functions.v"
`include "c_constants.v"
`include "rtr_constants.v"
      
   	//---------------------------------------------------------------------------
   	// parameters
   	//---------------------------------------------------------------------------
   
   	// number of VCs
   	parameter 	num_vcs = 4;
   
   	// select type of flow control
   	parameter 	flow_ctrl_type = `FLOW_CTRL_TYPE_CREDIT;
   
   	parameter 	reset_type = `RESET_TYPE_ASYNC;
      
   	//---------------------------------------------------------------------------
   	// derived parameters
   	//---------------------------------------------------------------------------
   
   	// width required to select individual VC
   	localparam 	vc_idx_width = clogb(num_vcs);
   
   	// width of flow control signals
   	localparam 	flow_ctrl_width = (1 + vc_idx_width);
   
   
   	//---------------------------------------------------------------------------
   	// interface
   	//---------------------------------------------------------------------------
   
   	input 				clk;
   	input 				reset;
	input 				active;
   
   	// flow control event valid
   	input 				fc_event_valid_in;
   
   	// flow control event VC selector
   	input	[0:num_vcs-1] 		fc_event_sel_in_ivc;
   
   	// outgoing flow control signals
   	output 	[0:flow_ctrl_width-1] 	flow_ctrl_out;
   	wire 	[0:flow_ctrl_width-1] 	flow_ctrl_out;
   
   
   	//---------------------------------------------------------------------------
   	// implementation
   	//---------------------------------------------------------------------------
   
   	generate
      	     
		wire cred_valid;
	     	assign cred_valid = fc_event_valid_in;
	     
	     	wire cred_valid_q;
	     
	     	// NOTE: This register needs to be cleared after a credit has been 
	     	// transmitted; consequently, we must extend the activity period 
	     	// accordingly. As creating a separate clock gating domain for a 
	     	// single register is liekly inefficient, this will probably mean 
	     	// that it will end up being free-running. If we could change 
	     	// things such that credits are transmitted using edge-based 
	     	// signaling (i.e., transitions), this could be avoided.
	     
	     	wire cred_active;
	     	assign cred_active = active | cred_valid_q;
	     
	     	wire cred_valid_s;
	     	assign cred_valid_s = cred_valid;
	     	
		c_dff
	       	#(	.width(1),
		 	.reset_type(reset_type))
	     	cred_validq
	       	(	.clk(clk),
			.reset(reset),
			.active(cred_active),
			.d(cred_valid_s),
			.q(cred_valid_q));
	     
	     	assign flow_ctrl_out[0] = cred_valid_q;
	     
	     	if(num_vcs > 1)
	       	begin
		  
			wire [0:vc_idx_width-1] cred_vc;
		  
			c_encode
		    	#(	.num_ports(num_vcs))
		  	cred_vc_enc
		    	(	.data_in(fc_event_sel_in_ivc),
		     		.data_out(cred_vc));
		  
		  	wire [0:vc_idx_width-1] cred_vc_s, cred_vc_q;
		  
			assign cred_vc_s = cred_vc;
		  
			c_dff
		    	#(	.width(vc_idx_width),
		      		.reset_type(reset_type))
		  	cred_vcq
		    	(	.clk(clk),
		     		.reset(reset),
		     		.active(cred_active),
		     		.d(cred_vc_s),
		     		.q(cred_vc_q));
		  
		  	assign flow_ctrl_out[1:1+vc_idx_width-1] = cred_vc_q;
		  
	   	end
	      
   	endgenerate
   
endmodule
